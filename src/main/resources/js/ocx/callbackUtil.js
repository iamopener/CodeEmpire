/**
 * 添加ocx事件返回脚本
 */

const setAttr = (attr, val) => {
	let obj = document.createAttribute(attr);
	obj.value = val;
	return obj;
};

const body = document.getElementsByTagName('body')[0];

let registerCallback = (clsid) => {
	let obj = document.createElement('script');
	let objId = setAttr('for', clsid);
	obj.setAttributeNode(objId);
	let event = setAttr('event', `OnAsynExecuted(p1, p2, p3, p4);`);
	obj.setAttributeNode(event);
	// obj.event = 'OnAsynExecuted(p1, p2, p3, p4)';
	let scriptContent =
	`console.log("监听到 OnAsynExecuted 事件", p1, p2, p3, p4);
	window.cwCallback(p1, p2, p3, p4);`;// 这里绑定CW_XFSControl的异步事件回调函数，其他地方在操作事件时绑定回调函数到cwCallBack
	obj.appendChild(document.createTextNode(scriptContent));
	console.log('执行了注册');
	body.appendChild(obj);
};

registerCallback('CW_XFSControl');
