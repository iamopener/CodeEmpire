/**
 * 用于加载ocx（生成并添加<object>）
 */

const setAttr = (attr, val) => {
	let obj = document.createAttribute(attr);
	obj.value = val;
	return obj;
};

const body = document.getElementsByTagName('body')[0];

let loadOCX = (id, clsid) => {
	let obj = document.createElement('object');
	let objId = setAttr('id', id);
	obj.setAttributeNode(objId);
	let objClassid = setAttr('classid', `clsid:${clsid}`);
	obj.setAttributeNode(objClassid);
	let objStyle = setAttr('style', 'display:none');
	obj.setAttributeNode(objStyle);
	let objType = setAttr('type', 'application/x-itst-activex');
	obj.setAttributeNode(objType);
	body.appendChild(obj);
};

loadOCX('fileocx', '72E563B2-41F5-437F-AF9D-FA613BFCEECF');
loadOCX('logocx', '83AE9880-5653-4DBF-86B0-08102ED3AD72');
loadOCX('CW_XFSControl', '67C20063-9E67-4925-B7F5-98053EA5BBA6');
