package t.i;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author ZYC
 * @since 2020/7/8 14:37
 */
@SpringBootApplication
public class CodeEmpireApplication {
    public static void main(String[] args) {
        SpringApplication.run(CodeEmpireApplication.class, args);
    }
}
