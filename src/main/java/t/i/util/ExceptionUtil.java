package t.i.util;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * @author ZYC
 * @since 2020/7/8 14:13
 */
public class ExceptionUtil {
    /**
     * 收集报错信息到字符串
     *
     * @param e 异常
     * @return 全部报错信息
     */
    public static String collectStackTrace(Exception e) {
        try (StringWriter stringWriter = new StringWriter();
             PrintWriter printWriter = new PrintWriter(stringWriter)) {
            e.printStackTrace(printWriter);
            printWriter.flush();
            stringWriter.flush();
            return stringWriter.toString();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return "";
    }
}
