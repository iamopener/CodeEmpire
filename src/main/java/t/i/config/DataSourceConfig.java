package t.i.config;

import com.alibaba.druid.pool.DruidDataSource;
import com.baomidou.mybatisplus.extension.spring.MybatisSqlSessionFactoryBean;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;

/**
 * @author ZYC
 * @since 2020/7/8 14:18
 */

//@Configuration
@Slf4j
//@MapperScan(basePackages = PgsqlDataSourceConfig.PACKAGE, sqlSessionFactoryRef = "pgSqlSessionFactory")
public class DataSourceConfig {

    @Value("${mybatis.type-alias-package}")
    private String typeAliasPackage;
    @Value("${mybatis.mapper-path}")
    private String mapperLocation;
    @Value("${mybatis.mybatis-config-file}")
    private String mybatisConfigFile;

    @Value("${spring.datasource.url}")
    private String url;
    @Value("${spring.datasource.username}")
    private String user;
    @Value("${spring.datasource.password}")
    private String password;
    @Value("${spring.datasource.driver-class-name}")
    private String driverClass;

    /////////////////////druid参数///////////////////////////////////////////////////
    @Value("${pgsql.datasource.maxActive}")
    private String maxActive;
    @Value("${pgsql.datasource.initialSize}")
    private String initialSize;
    @Value("${pgsql.datasource.maxWait}")
    private String maxWait;
    @Value("${pgsql.datasource.minIdle}")
    private String minIdle;
    @Value("${pgsql.datasource.timeBetweenEvictionRunsMillis}")
    private String timeBetweenEvictionRunsMillis;
    @Value("${pgsql.datasource.minEvictableIdleTimeMillis}")
    private String minEvictableIdleTimeMillis;
    @Value("${pgsql.datasource.validationQuery}")
    private String validationQuery;
    @Value("${pgsql.datasource.testWhileIdle}")
    private String testWhileIdle;
    @Value("${pgsql.datasource.testOnBorrow}")
    private String testOnBorrow;
    @Value("${pgsql.datasource.testOnReturn}")
    private String testOnReturn;
    @Value("${pgsql.datasource.poolPreparedStatements}")
    private String poolPreparedStatements;
    @Value("${pgsql.datasource.maxOpenPreparedStatements}")
    private String maxOpenPreparedStatements;

    @Bean(name = "pgDataSource")
    public DataSource pgDataSource() {
        DruidDataSource druidDataSource = new DruidDataSource();
        druidDataSource.setDriverClassName(driverClass);
        druidDataSource.setUrl(url);
        druidDataSource.setUsername(user);
        druidDataSource.setPassword(password);
        druidDataSource.setMaxActive(Integer.parseInt(maxActive));
        druidDataSource.setInitialSize(Integer.parseInt(initialSize));
        druidDataSource.setMaxWait(Integer.parseInt(maxWait));
        druidDataSource.setMinIdle(Integer.parseInt(minIdle));
        druidDataSource.setTimeBetweenEvictionRunsMillis(Integer.parseInt(timeBetweenEvictionRunsMillis));
        druidDataSource.setMinEvictableIdleTimeMillis(Integer.parseInt(minEvictableIdleTimeMillis));
        druidDataSource.setValidationQuery(validationQuery);
        druidDataSource.setTestWhileIdle(Boolean.parseBoolean(testWhileIdle));
        druidDataSource.setTestOnBorrow(Boolean.parseBoolean(testOnBorrow));
        druidDataSource.setTestOnReturn(Boolean.parseBoolean(testOnReturn));
        druidDataSource.setPoolPreparedStatements(Boolean.parseBoolean(poolPreparedStatements));
        druidDataSource.setMaxOpenPreparedStatements(Integer.parseInt(maxOpenPreparedStatements));
        return druidDataSource;
    }

    @Bean(name = "pgTransactionManager")
    public DataSourceTransactionManager pgTransactionManager() {
        return new DataSourceTransactionManager(pgDataSource());
    }

    @Bean(name = "pgSqlSessionFactory")
    public MybatisSqlSessionFactoryBean clusterSqlSessionFactory(@Qualifier("pgDataSource") DataSource pgDataSource) throws Exception {
        final MybatisSqlSessionFactoryBean sessionFactory = new MybatisSqlSessionFactoryBean();
        sessionFactory.setConfigLocation(new ClassPathResource(mybatisConfigFile));
//        sessionFactory.setMapperLocations(new PathMatchingResourcePatternResolver()
//                .getResources(ResourcePatternResolver.CLASSPATH_ALL_URL_PREFIX + mapperLocation));
        PathMatchingResourcePatternResolver pathMatchingResourcePatternResolver = new PathMatchingResourcePatternResolver();
        String packageSearchPath = ResourcePatternResolver.CLASSPATH_ALL_URL_PREFIX + mapperLocation;
        log.debug("pakcage:" + packageSearchPath);
        sessionFactory.setMapperLocations(pathMatchingResourcePatternResolver.getResources(packageSearchPath));
        sessionFactory.setDataSource(pgDataSource);
        sessionFactory.setTypeAliasesPackage(typeAliasPackage);
        return sessionFactory;
    }
}
